var express = require('express');
var router = express.Router();

const { Pool } = require('pg');

const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'nodepostgres',
  password: 'DinhAn591',
  port: 5432,
});

/* GET home page. */
router.get('/', async function(req, res, next) {
  try {
    const result = await pool.query('SELECT * FROM CONTACT');
    console.log(result.rows[0]);
    res.render('index', { title: 'index' });
  } catch (err) {
    console.error(err);
    res.send('Error fetching data');
  }
});



/* GET thêm page. */
router.get('/them', async function(req, res, next) {
  res.render('them', { title: 'them' });
});

/* POST thêm page. */
router.post('/them', async function(req, res, next) {
  var ten = req.body.ten;
  var tuoi = req.body.tuoi;
  try {
    await pool.query('INSERT INTO contact(ten, tuoi) VALUES ($1, $2)', [ten, tuoi]);
    res.redirect('/them'); // Redirect to the /them page after inserting data
  } catch (err) {
    console.error(err);
    res.send('Error inserting data');
  }
});


module.exports = router;
